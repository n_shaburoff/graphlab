#pragma once
#include <iostream>
#include <vector>
class Node
{
public:
	Node();
	Node(float x, float y, std::string name = "");
	Node(Node* node);
	float getX();
	float getY();
	std::string getName();
	void setX(float x);
	void setY(float y);
	void setName(std::string name);
	void addNeighbor(Node* node);
	std::vector<Node*> getNeighbors();
	float distance(Node* neighbor);
private:
	float x_;
	float y_;
	std::string name_;
	std::vector<Node*> neighbors_;
};