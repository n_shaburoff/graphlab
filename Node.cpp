#include "Node.h"

Node::Node()
{
	setX(0);
	setY(0);
	setName("");
}

Node::Node(float x, float y, std::string name)
{	
	setX(x);
	setY(y);
	setName(name);
}

Node::Node(Node* node)
{
	setX(node->getX());
	setY(node->getY());
	setName(node->getName());
}

float Node::getX()
{
	return x_;
}

float Node::getY()
{
	return y_;
}

std::string Node::getName()
{
	return name_;
}

void Node::setX(float x)
{
	x_ = x;
}

void Node::setY(float y)
{	
	y_ = y;
}

void Node::setName(std::string name)
{
	name_ = name;
}

void Node::addNeighbor(Node* node)
{
	// Add neighbor node to this->neighbors_ if not exist,
	// and add this to the neighbor node->neighbors_
	for (Node* neighbor : neighbors_)
		if (neighbor == node) return;
	neighbors_.push_back(node);
	node->addNeighbor(this);
}

std::vector<Node*> Node::getNeighbors()
{
	return neighbors_;
}

float Node::distance(Node* neighbor)
{
	return sqrt(pow(x_ - neighbor->getX(), 2) + pow(y_ - neighbor->getY(), 2));
}
