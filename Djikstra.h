#include "Node.h"
#include <vector>
#include <string>

#define large_number 99999

class NodeDj : public Node {
public:
	NodeDj(float x, float y, std::string name = "") : Node(x, y, name) {}
	float tnDist();
	void setTnDist(float newTnDist);
	bool visited();
	void setVisited(bool newVisited);
	NodeDj* prev();
	void setPrev(NodeDj* newPrev);
	std::vector<NodeDj*> neighbors();
	void addNeighbor(NodeDj* node);
private:
	float tnDist_ = large_number;
	bool visited_ = false;
	NodeDj* prev_ = NULL;
	std::vector<NodeDj*> neighbors_;
};