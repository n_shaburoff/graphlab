#include "Djikstra.h"

float NodeDj::tnDist()
{
	return tnDist_;
}

void NodeDj::setTnDist(float newTnDist)
{
	tnDist_ = newTnDist;
}

bool NodeDj::visited()
{
	return visited_;
}

void NodeDj::setVisited(bool newVisited)
{
	visited_ = newVisited;
}

NodeDj* NodeDj::prev()
{
	return prev_;
}

void NodeDj::setPrev(NodeDj* newPrev)
{
	prev_ = newPrev;
}

std::vector<NodeDj*> NodeDj::neighbors()
{
	return neighbors_;
}

void NodeDj::addNeighbor(NodeDj* node)
{
	for (NodeDj* neighbor : neighbors_)
		if (neighbor == node) return;
	neighbors_.push_back(node);
	node->addNeighbor(this);
}
