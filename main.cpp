//#include <iostream>
//#include <SFML/Graphics.hpp>
//#include "Node.h"
//#include "graphBuilder.h"
//
//int main()
//{
//	const int width = 800;
//	const int height = 600;
//
//	const int n_nodes = 6;
//
//	Node* nodeList[n_nodes];
//	fillGraph<Node>(nodeList, n_nodes, width, height, 2, 3);
//
//	sf::CircleShape nodeCircles[n_nodes];
//	for (int i = 0; i < n_nodes; ++i)
//	{
//		float radius = 10;
//		nodeCircles[i].setRadius(radius);
//		nodeCircles[i].setPosition(sf::Vector2f(nodeList[i]->getX() - radius, nodeList[i]->getY() - radius));
//		nodeCircles[i].setFillColor(sf::Color(0, 0, 0, 30));
//	}
//
//	sf::ContextSettings settings;
//	settings.antialiasingLevel = 8;
//	sf::RenderWindow window(sf::VideoMode(width, height), "Graph", sf::Style::Default, settings);
//	while (window.isOpen())
//	{
//		sf::Event event;
//		while (window.pollEvent(event))
//		{
//			if (event.type == sf::Event::Closed)
//			{
//				window.close();
//			}
//		}
//
//		window.clear(sf::Color::White);
//		for (int i = 0; i < n_nodes; ++i)
//		{
//			window.draw(nodeCircles[i]);
//			for (Node* neighbor : nodeList[i]->getNeighbors())
//			{
//				sf::RectangleShape line = Line(nodeList[i]->getX(), nodeList[i]->getY(), neighbor->getX(), neighbor->getY());
//				window.draw(line);
//			}
//		}
//
//		window.display();
//	}
//}